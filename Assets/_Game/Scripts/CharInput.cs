﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharController))]
public class CharInput : MonoBehaviour
{
    private CharController controller = null;

    private void Start()
    {
        controller = GetComponent<CharController>();
    }

    private void Update()
    {
        controller.Move(Input.GetAxisRaw("Horizontal"));
        if (Input.GetButtonDown("Jump"))
            controller.Jump();
    }
}
