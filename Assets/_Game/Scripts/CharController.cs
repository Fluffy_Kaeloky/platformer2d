﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class CharController : MonoBehaviour
{
    public float gravityMultiplier = 1.0f;

    public float maxSpeed = 5.0f;
    public float acceleration = 10.0f;
    public float breakForce = 20.0f;

    public bool canDoubleJump = true;
    public bool canAirControl = true;
    public float jumpForce = 20.0f;

    public bool flipGraphics = true;

    [Range(1.0f, 90.0f)]
    public float maxGroundAngle = 40.0f;

    private SpriteRenderer sprite = null;

    private Vector2 velocity = Vector2.zero;

    public bool IsGrounded { get { return isGrounded; } }
    private bool isGrounded = false;

    public Vector2 GroundNormal { get { return groundNormal; } }
    private Vector2 groundNormal = Vector3.zero;

    private bool didDoubleJump = false;

    private new BoxCollider2D collider = null;
    private Vector2[] raycastsLocations = null;

    private float accelerationInput = 0.0f;

    private void Start()
    {
        collider = GetComponent<BoxCollider2D>();

        raycastsLocations = new Vector2[RaycastLocations.count];

        Vector2 scaledOffset = Vector2.Scale(collider.offset, transform.lossyScale2d());

        raycastsLocations[(int)RaycastLocation.GroundLeft] = scaledOffset + new Vector2((-collider.size.x * transform.lossyScale.x) / 2.0f, 0.0f);
        raycastsLocations[(int)RaycastLocation.GroundRight] = scaledOffset + new Vector2((collider.size.x * transform.lossyScale.x) / 2.0f, 0.0f);
        raycastsLocations[(int)RaycastLocation.WallLeftDown] = scaledOffset + new Vector2(0.0f, (-collider.size.y * transform.lossyScale.y) / 2.0f);
        raycastsLocations[(int)RaycastLocation.WallLeftUp] = scaledOffset + new Vector2(0.0f, (collider.size.y * transform.lossyScale.y) / 2.0f);
        raycastsLocations[(int)RaycastLocation.WallRightDown] = scaledOffset + new Vector2(0.0f, (-collider.size.y * transform.lossyScale.y) / 2.0f);
        raycastsLocations[(int)RaycastLocation.WallRightUp] = scaledOffset + new Vector2(0.0f, (collider.size.y * transform.lossyScale.y) / 2.0f);
        raycastsLocations[(int)RaycastLocation.CeilLeft] = scaledOffset + new Vector2((-collider.size.x * transform.lossyScale.x) / 2.0f, 0.0f);
        raycastsLocations[(int)RaycastLocation.CeilRight] = scaledOffset + new Vector2((collider.size.x * transform.lossyScale.x) / 2.0f, 0.0f);
    }

    private void FixedUpdate()
    {
        //Ground checking
        if (velocity.y <= 0.0f)
        {
            for (int i = 0; i < 2; i++)
            {
                RaycastHit2D[] hits;
                Debug.DrawLine(transform.position2d() + raycastsLocations[(int)RaycastLocation.GroundLeft + i], transform.position2d() + raycastsLocations[(int)RaycastLocation.GroundLeft + i] + RaycastLocations.ToNormal(RaycastLocation.GroundLeft + i) * ((collider.size.y * transform.lossyScale.y) / 2.0f), Color.red);
                if ((hits = Physics2D.RaycastAll(transform.position2d() + raycastsLocations[(int)RaycastLocation.GroundLeft + i], RaycastLocations.ToNormal(RaycastLocation.GroundLeft + i), (collider.size.y * transform.lossyScale.y) / 2.0f)).Length != 0)
                {
                    foreach (var hit in hits)
                    {
                        if (hit.collider == collider)
                        {
                            if (hits.Length == 1)
                            {
                                isGrounded = false;
                                groundNormal = Vector2.zero;
                                break;
                            }

                            continue;
                        }

                        if (Vector2.Dot(Vector2.up, hit.normal) >= (maxGroundAngle / 90.0f))
                        {
                            isGrounded = true;
                            groundNormal = hit.normal;

                            velocity.y = 0.0f;

                            transform.position = new Vector3(transform.position.x, hit.point.y + ((collider.size.y * transform.lossyScale.y) / 2.0f), transform.position.z);

                            didDoubleJump = false;

                            break;
                        }
                    }
                }
            }
        }

        //Wall checking


        if (!isGrounded)
            velocity += Physics2D.gravity * Time.fixedDeltaTime * gravityMultiplier;

        if (canAirControl || isGrounded)
        {
            if (Mathf.Sign(velocity.x) == accelerationInput || velocity.x == 0.0f)
                velocity.x = Mathf.Max(Mathf.Min(velocity.x + accelerationInput * acceleration * Time.fixedDeltaTime, maxSpeed), -maxSpeed);
            else
            {
                if (velocity.x > 0.0f)
                    velocity.x = Mathf.Max(velocity.x + -Mathf.Sign(velocity.x) * breakForce * Time.fixedDeltaTime, 0.0f);
                else
                    velocity.x = Mathf.Min(velocity.x + -Mathf.Sign(velocity.x) * breakForce * Time.fixedDeltaTime, 0.0f);
            }

        }

        float angle = 0.0f;
        if (isGrounded && groundNormal != Vector2.zero)
            angle = Vector2.Angle(Vector2.up, groundNormal);

        Debug.Log("angle : " + angle);
        Debug.Log("groundNormal : " + groundNormal);

        Vector2 rotatedVelocity = Quaternion.Euler(0.0f, 0.0f, angle) * velocity;

        transform.Translate(new Vector3(rotatedVelocity.x * Time.fixedDeltaTime, rotatedVelocity.y * Time.fixedDeltaTime, 0.0f));
    }

    public void Move(float direction)
    {
        accelerationInput = direction;
    }

    public void Jump()
    {
        if (isGrounded || (canDoubleJump && !didDoubleJump))
        {
            if (!isGrounded && canDoubleJump)
                didDoubleJump = true;

            isGrounded = false;
            velocity.y += jumpForce;
        }
    }

    public enum RaycastLocation : int
    {
        GroundLeft = 0,
        GroundRight = 1,
        WallLeftDown = 2,
        WallLeftUp = 3,
        WallRightDown = 4,
        WallRightUp = 5,
        CeilLeft = 6,
        CeilRight = 7
    }
    public static class RaycastLocations
    {
        public const int count = 8;

        public static Vector2 ToNormal(RaycastLocation location)
        {
            switch (location)
            {
                case RaycastLocation.GroundLeft:
                    return Vector2.down;
                case RaycastLocation.GroundRight:
                    return Vector2.down;
                case RaycastLocation.WallLeftDown:
                    return Vector2.left;
                case RaycastLocation.WallLeftUp:
                    return Vector2.left;
                case RaycastLocation.WallRightDown:
                    return Vector2.right;
                case RaycastLocation.WallRightUp:
                    return Vector2.right;
                case RaycastLocation.CeilLeft:
                    return Vector2.up;
                case RaycastLocation.CeilRight:
                    return Vector2.up;

                default:
                    return Vector2.zero;
            }
        }
    }

}
