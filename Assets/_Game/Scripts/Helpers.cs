﻿using UnityEngine;
using System.Collections;

public static class Extensions
{
    public static Vector2 position2d(this UnityEngine.Transform transform)
    {
        return new Vector2(transform.position.x, transform.position.y);
    }

    public static Vector2 lossyScale2d(this UnityEngine.Transform transform)
    {
        return new Vector2(transform.lossyScale.x, transform.lossyScale.y);
    }
}
